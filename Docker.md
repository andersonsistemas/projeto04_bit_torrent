Funcionamento Através do Docker
===============================

`docker pull asapach/peerflix-server`

É necessário verificar se você possui permissões de gravação no seu diretório de destino `/tmp/torrent-stream`:

```sh
mkdir /tmp/torrent-stream
chmod a+rw /tmp/torrent-stream/
```
Comandos para execução do peerflix-server

`docker run -p 9000:9000 -p 6881:6881 -p 6881:6881/udp -d -v /tmp/torrent-stream:/tmp/torrent-stream asapach/peerflix-server`

Ou

`docker run -p 9000:9000 -p 6881:6881 -p 6881:6881/udp --rm -v /tmp/torrent-stream:/tmp/torrent-stream asapach/peerflix-server`
