# Projeto 04 - Individual - Sobre Aplicação Peer-to-Peer - Disciplina Aplicações Distribuídas

## Aluno: Anderson Martins Dias


### Experimentação sobre o uso do protocolo BitTorrent em uma aplicação que realiza streaming de vídeo a partir de arquivos .torrent ou (magnet links) links magnéticos.

#### Foram utilizados como base as aplicações PeerFlix e PeerFlix-Server ambas baseadas no protocolo BitTorrent

Realizei uma documentação sobre o protocolo [BitTorrent](https://gitlab.com/andersonsistemas/projeto04_bit_torrent/blob/master/BitTorrent.MD).
Onde foi explorado o princípio de funcionamento do protocolo, suas referências técnicas, formas de utilização do protocolo.

Todos os testes foram [documentados](https://gitlab.com/andersonsistemas/projeto04_bit_torrent/blob/master/Projeto_4.md) em imagens e vídeos, pode ser conferido a utilização das duas aplicações peerflix e peerflix-server, instalação, configuração, etc.


