# Utilização das aplicações Peerflix e Peerflix Server


## Peerflix-Server

![alt text](https://image.ibb.co/gF7iCv/peerflix.png)



#### Cliente Torrent Streaming  para node.js com interface gráfica web.

Para desenvolvedores o código fonte está hospedado neste diretório [github](https://github.com/asapach/peerflix-server.git).

Se dejesa efetuar o clone do projeto **peerflix-server** e não sabe como proceder, pode clicar [aqui](https://git-scm.com/book/pt-br/v1/Git-Essencial-Obtendo-um-Reposit%C3%B3rio-Git) e obtenha informações detalhadas de como proceder.<br>
Para clonar o projeto utilizamos o seguinte comando:<br>
`$ git clone https://github.com/asapach/peerflix-server.git`

### Pré-requisitos

Se quisermos construir e executar o código fonte, após o clone do projeto no link acima,
iremos precisar dos módulos **Grunt** e **Bower**:

O **Grunt** é um módulo que  permite automatizar tarefas no terminal, concatenar arquivos de JavaScript e CSS, executar o **JSHint**, dentre muitas outras coisas.
A grande vantagem de se usar o Grunt.js em vez de outras alternativas é a grande quantidade de plugins disponíveis. A lista de [plugins oficiais](https://gruntjs.com/plugins) é bastante grande, mas também existem centenas de plugins criados pela comunidade.
**JSHint** é uma ferramenta busca tanto erros de sintaxe, como erros estruturais, exibindo-as no prompt. Desenvolvida por Douglas Crockford.
O **Bower** é um [projeto](https://bower.io/) que permite gerenciar dependências client-side, ou seja, no lado cliente da aplicação, de uma maneira bastante simples. Em vez de ter que entrar no site de cada projeto que você quer usar e baixar os arquivos necessários, você pode automatizar este processo criando um arquivo de manifesto.


Instalando o Grunt e  o Bower: 
`$ npm install -g grunt-cli bower`

Instale todos os módulos locais:
`$ npm install`

Instale todas as dependências externas:
`$ bower install`

### Execução do Servidor

`$ grunt serve`

A tarefa primeiro realizará todos os pré-processamentos necessários, podemos habilitar o relógio, realizar um reload do projeto, configurar jsint e karma, para depois abrir o navegador. Se você não quiser entrar nos detalhes das configurações, você pode simplesmente executar 

`$ npm start`

### Envio para Produção

`$ grunt build`

Esta tarefa otimizará e minimizará todos os recursos e preparará o código para implantação (por exemplo, no registro npm). Você pode executar a versão de produção localmente usando 
`$ grunt serve:dist`

### Realização de Testes Unitários

`$ grunt test`

Esta tarefa iniciará o corredor de teste Karma e reportará os resultados.

### JSHint

`$ grunt jshint`

Esta tarefa executará análise de código e reportará os resultados.



A aplicação peerflix-server foi baseada na torrent-stream, e inspirada pelo peerflix.
Na imagem abaixo podemos observar a aplicação em funcionamento.

![alt text](https://cdn.rawgit.com/asapach/peerflix-server/master/capture.gif)


#### Instalação:

`$ npm install -g peerflix-server`<br>
`$ peerflix-server`<br>
Abra o seu navegador no seguinte endereço:`http://localhost:9000/`

![alt text](https://preview.ibb.co/jK8bza/000_peerflix_server.png)

![alt text](https://preview.ibb.co/dbMwza/001_peerflix_server.png)

#### Configuração:

Você pode configurar a aplicação através do arquivo .json no seguinte diretório `~/.config/peerflix-server/config.json` (este arquivo não é criado por padrão). As modificações serão refletidas em todas instâncias dos torrent-stream. Abaixo está um exemplo de como modificar as configurações padrão:

```javascript
{
  "connections": 50,
  "tmp": "/mnt/torrents"
}
```
![alt text](https://preview.ibb.co/dKwUKa/002_peerflix_server.png)


Também é possível alterar a porta de comunicação padrão, modificando a configuração da variável de ambiente **PORT**:

`PORT=1234 peerflix-server`

Caso seja o Sistema Operacional Windows

`SET PORT=1234`
`peerflix-server`

A aplicação armazena o seu estado atual (lista de torrents) no seguinte arquivo `~/.config/peerflix-server/torrents.json`

#### Utilizando a aplicação em modo Daemon (segundo plano)

Para utilizar a aplicação peerflix-server no modo [daemon](https://pt.wikipedia.org/wiki/Daemon_(computa%C3%A7%C3%A3o)), podemos utilizar a aplicação [forever](https://www.npmjs.com/package/forever):

`$ npm install -g forever`<br>
`$ forever start $(which peerflix-server)`

Para conhecer maiores detalhes, e comandos dessa ferramenta podemos conferi-la [aqui](https://github.com/foreverjs/forever#command-line-usage).




As aplicações são similares, baseadas na aplicação [torrent-stream](https://github.com/mafintosh/torrent-stream), utilizam o protocolo p2p [BitTorrent](https://gitlab.com/andersonsistemas/projeto04_bit_torrent/blob/master/BitTorrent.MD) para realização de Streaming de Áudio/Vídeo.

## Peerflix

Para instalação da peerflix

O diretório do projeto para desenvolvedores é:

`https://github.com/mafintosh/peerflix`


Instalação através dos módulos de instalação do **"NodeJS"**: 

Devemos ter o nodejs instalado, que podemos realizar através do comando:

`$ apt-get install nodejs`

A seguir devemos instalar a aplicação npm (gerenciador de pacotes) para instalação das dependências em aplicações nodejs, que pode ser realizado:

`$ apt-get install npm`

Para instalar o peerflix, utilizamos o seguinte comando:

`$ npm install -g peerflix`


Também pode ser utilizado através de um [add-on](https://addons.mozilla.org/en-US/firefox/addon/watch-with-peerflix/) do navegador Mozilla Firefox.

#### Utilização do peerflix 

Peerflix pode ser utilizado através de um  link magnético ou um arquivo torrent. Para transmitir um vídeo com seu link magnético, use o seguinte comando.

`$ peerflix "magnet:?xt=urn:btih:ef330b39f4801d25b4245212e75a38634bfc856e" --vlc`

![alt text](https://preview.ibb.co/nbcsL5/Captura_de_tela_de_2017_07_01_00_28_56.png)


Devemos inserir aspas duplas "  antes e depois do link magnético, pois eles geralmente contêm o caracter &. peerflix. Será exibida uma interface de terminal. A primeira linha contém um endereço para um servidor http. Ao utilizar --vlc como argumento no final do comando, isso garantirá que o vlc seja aberto quando o torrent estiver pronto para transmitir.

![alt text](https://preview.ibb.co/m9OotQ/Captura_de_tela_de_2017_07_01_00_29_22.png)
![alt text](https://preview.ibb.co/j0WK05/Captura_de_tela_de_2017_07_01_00_29_18.png)
![alt text](https://preview.ibb.co/nCtotQ/Captura_de_tela_de_2017_07_01_00_29_37.png)


Para realizar um streaming de música utilizando um arquivo torrent podemos utilizar o seguinte comando.

` $ peerflix " http: //some-torrent/music.torrent " -a --vlc`

O argumento "-a" garante que todos os arquivos no repositório de música sejam reproduzidos com vlc. Caso contrário, se o torrent possuir diversos arquivos, a aplicação peerflix escolherá o maior arquivo. Para obter uma lista completa das opções disponíveis, podemos executar o peerflix com o argumento de ajuda.

`$ peerflix --help`

Alguns exemplos de comandos utilizados:

`$ peerflix magnet-link --list # Select from a list of files to download`<br>
`$ peerflix magnet-link --vlc -- --fullscreen # will pass --fullscreen to vlc`<br>
`$ peerflix magnet-link --mplayer --subtitles subtitle-file.srt # play in mplayer with subtitles`<br>
`$ peerflix magnet-link --connection 200 # set max connection to 200`<br>

Usuários do Chromebook

Os Chromebooks estão configurados para recusar todas as conexões recebidas por padrão: Podemos reverter isso desta forma:

`$ sudo iptables -P INPUT ACCEPT`





### Vídeos com a demonstração da utilização das ferramentas Peerflix e Peerflix-Server

#### Peerflix

<a href="http://www.youtube.com/watch?feature=player_embedded&v=N8RuTCO_cF4
" target="_blank"><img src="http://img.youtube.com/vi/N8RuTCO_cF4/0.jpg" 
alt="IMAGE ALT TEXT HERE" width="240" height="180" border="10" /></a>

>Uma outra demonstração utilizando Linux Mint<br>
<a href="http://www.youtube.com/watch?feature=player_embedded&v=BxHaEXQnlNE
" target="_blank"><img src="http://img.youtube.com/vi/BxHaEXQnlNE/0.jpg" 
alt="IMAGE ALT TEXT HERE" width="240" height="180" border="10" /></a>


#### Peerflix Server
<a href="http://www.youtube.com/watch?feature=player_embedded&v=DFzb4Aeb5f4
" target="_blank"><img src="http://img.youtube.com/vi/DFzb4Aeb5f4/0.jpg" 
alt="IMAGE ALT TEXT HERE" width="240" height="180" border="10" /></a>


### Referências:

https://www.npmjs.com/package/peerflix

https://www.npmjs.com/package/peerflix-server
