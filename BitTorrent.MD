### O que é BitTorrent?
 
BitTorrent (BTP) é um protocolo para a distribuição de arquivos de forma colaborativa pela Internet e teve a sua implantação na Internet no ano 2002. É um protocolo projetado para distribuir arquivos grandes em peças utilizando distribuição mútua das peças entre um conjunto de pares(peers) chamado enxame(swarm). Ele é melhor classificado como um protocolo peer-to-peer (P2P), embora também contenha elementos altamente centralizados. Peer-to-peer ou P2P (do inglês par-a-par ou simplesmente ponto-a-ponto, com sigla P2P) é uma arquitetura de redes de computadores onde cada um dos pontos ou nós da rede funciona tanto como cliente quanto como servidor, permitindo compartilhamentos de serviços e dados sem a necessidade de um servidor central. Por não se basear em uma arquitetura cliente-servidor, onde apenas o servidor é responsável pela execução de todas as funções da rede, o P2P tem uma enorme vantagem justamente por não depender de um servidor e de todos os nós estarem interconectados permitindo o acesso a qualquer nó de qualquer nó. Por esse motivo a rede tem uma elevada disponibilidade.
O BTP já foi implementado diversas vezes em diferentes plataformas, e devido à sua expansão e utilização poderíamos afirmar que ele seja um protocolo maduro, embora lhe falte uma descrição formalizada, detalhada e completa até o momento.
O BTP foi concebido e implementado por Bram Cohen como um substituto P2P para o protocolo FTP padrão, sua idéia era para ser utilizado em locais onde o uso de uma implementação em FTP coloca muita pressão no servidor em termos de processamento de pedidos e largura de banda. Normalmente, um cliente não usa sua capacidade de upload durante o download de um arquivo. A abordagem BTP capitaliza esse fato ao fazer com que os clientes enviem bits dos dados entre si. Em comparação com FTP, isso acrescenta grande escalabilidade e vantagens de gerenciamento de custos.
 
## Terminologia
 
#### Peer:
Um peer é um nó em uma rede que participa no compartilhamento de arquivos. Pode simultaneamente atuar como servidor e cliente para outros nós da rede.
 
#### Neighboring peers:
Peers para os quais um cliente possui um ponto ativo para apontar a conexão TCP.
 
#### Client:
Um cliente é um agente de usuário (UA) que atua como um parceiro em nome de um usuário.
 
#### Torrent:
Um torrent é o termo para o arquivo (torrent de arquivo único) ou grupo de arquivos (torrent de vários arquivos) que o cliente está baixando.
 
#### Swarm ou Enxame
Uma rede de pares que operam ativamente em um determinado torrent.
 
#### Seeder:
A peer that has a complete copy of a torrent
 
#### Tracker ou Rastreador
Um rastreador é um servidor centralizado que contém informações sobre um ou mais torrents e enxames associados. Ele funciona como um gateway para pares em um enxame.
 
#### Metainfo file:
Um arquivo de texto que contém informações sobre o torrent, por exemplo, o URL do rastreador. Geralmente tem a extensão .torrent.
 
#### Peer ID:
Uma seqüência de 20 bytes que identifica o par. Como o ID do peer é obtido está fora do escopo deste documento, mas um ponto deve certificar-se de que o ID do país que ele usa tem uma probabilidade muito alta de ser exclusivo no enxame.
 
#### Info hash:
Um hash SHA1 que identifica de maneira exclusiva o torrent. É calculado a partir de dados no arquivo metainfo.
 
 
 
## Como o BitTorrent funciona ?
Quando você acessa uma página da Web, você está realizando o download dessa página através do protocolo HTTP. Desta forma, o seu computador se conecta ao servidor da Web e baixa os dados diretamente desse servidor. Cada computador que baixa os dados o faz do servidor central da página. Isto é o quanto do tráfego na web funciona. 

![Alt text](http://ap.imagensbrasil.org/images/2017/06/30/1bittorrent_network-with-central-server.png)
 
O BitTorrent é um protocolo peer-to-peer, o que significa que os computadores em um "swarm" BitTorrent (um grupo de computadores que baixam e carregam o mesmo torrent) transferem dados entre eles sem a necessidade de um servidor central.

![Alt text](http://ap.imagensbrasil.org/images/2017/06/30/2bittorrent_peer-to-peer-network.png)
 
Tradicionalmente, um computador junta um enxame BitTorrent carregando um arquivo .torrent em um cliente BitTorrent. O cliente BitTorrent entra em contato com um tracker (rastreador) especificado no arquivo .torrent. O tracker é um servidor especial que acompanha os computadores conectados. O tracker compartilha seus endereços IP com outros clientes BitTorrent no enxame, permitindo que eles se conectem entre si.
Uma vez conectado, um cliente BitTorrent transfere bits dos arquivos no torrent em pequenas peças, baixando todos os dados que pode obter. Uma vez que o cliente BitTorrent possui alguns dados, pode então começar a enviar esses dados para outros clientes BitTorrent no enxame. Desta forma, todo mundo que está baixando um torrent também está carregando o mesmo torrent. Isso acelera a velocidade de download de todos. Se 10.000 pessoas estão baixando o mesmo arquivo, ele não coloca muito estresse em um servidor central. Em vez disso, cada downloader contribui com largura de banda de upload para outros downloaders, garantindo que o torrent permaneça rápido. A figura a  seguir ilustra como é realizada a distribuição das pequenas peças dos arquivos em transmissão na rede.

![Alt text](http://ap.imagensbrasil.org/images/2017/06/30/3bittorrent-swarm.png)

Importante, os clientes BitTorrent nunca realmente baixam arquivos do próprio rastreador. O rastreador participa do torrent apenas ao acompanhar os clientes BitTorrent conectados ao enxame, na verdade, não baixando ou enviando dados.

## Leechers e Seeders
Os usuários que baixam de um enxame BitTorrent são comumente chamados de "leechers" ou "pares". Usuários que permanecem conectados a um enxame BitTorrent mesmo depois de terem baixado o arquivo completo, contribuindo com mais de sua largura de banda de upload para que outras pessoas possam continuar a baixar o arquivo, são chamados de "semeadores". Para que um torrent seja transferível, uma semente - que tem uma cópia completa de todos os arquivos no torrent - deve inicialmente se juntar ao enxame para que outros usuários possam baixar os dados. Se uma torrente não tem semeadores, não será possível baixar - nenhum usuário conectado possui o arquivo completo.
Os clientes BitTorrent recompensam outros clientes que enviam, preferindo enviar dados para clientes que contribuem com mais largura de banda de upload em vez de enviar dados para clientes que carregam em uma velocidade muito baixa. Isso acelera os tempos de download para o enxame como um todo e recompensa os usuários que contribuem com mais largura de banda de upload.

![Alt text](http://ap.imagensbrasil.org/images/2017/06/30/4bittorrent_utorrent-peers-and-seeds.png)

## Torrent Trackers e Trackerless Torrents
Nos últimos tempos, um sistema torrent descentralizado "sem rastreadores" permite que os clientes BitTorrent se comuniquem entre si sem a necessidade de nenhum servidor central. Os clientes BitTorrent usam tecnologia de tabela hash distribuída (DHT) para isso, com cada cliente BitTorrent funcionando como um nó DHT. Quando você adiciona uma torrente usando um "link magnético", o nó DHT entra em contato com nós próximos e noutros nós em contato com outros nós até encontrar as informações sobre o torrent.
Como a especificação do protocolo DHT diz: "Com efeito, cada ponto se torna um rastreador". Isso significa que os clientes BitTorrent não precisam mais de um servidor central gerenciando um enxame. Em vez disso, o BitTorrent se torna um sistema de transferência de arquivos peer-to-peer completamente descentralizado.
A DHT também pode trabalhar com os rastreadores tradicionais. Por exemplo, um torrent pode usar o DHT e um rastreador tradicional, o que proporcionará redundância no caso do rastreador falhar.

![Alt text](http://ap.imagensbrasil.org/images/2017/06/30/5bittorrent_dht-in-utorrent.png)

## BitTorrent não é apenas para pirataria
O BitTorrent não é sinônimo de pirataria. A Blizzard usa um cliente BitTorrent personalizado para distribuir atualizações para seus jogos, incluindo World of Warcraft, StarCraft II e Diablo 3. Isso ajuda a acelerar downloads para todos, permitindo que as pessoas compartilhem sua largura de banda de upload com os outros, alavancando a largura de banda não utilizada para downloads mais rápidos para todos. Claro, também economiza dinheiro da Blizzard em suas contas de largura de banda.
As pessoas podem usar o BitTorrent para distribuir arquivos grandes para um número significativo de pessoas sem pagar a largura de banda de hospedagem na web. Um filme gratuito, um álbum de música ou um jogo podem ser hospedados no BitTorrent, permitindo um método de distribuição fácil e gratuito, onde as pessoas que baixam o arquivo também ajudam a distribuí-lo. WikiLeaks distribuiu dados via BitTorrent, tirando uma carga significativa de seus servidores. As distribuições Linux usam o BitTorrent para ajudar a distribuir suas imagens de disco ISO.
A BitTorrent, Inc. - uma empresa responsável pelo desenvolvimento do BitTorrent como protocolo, que também adquiriu e desenvolveu o popular cliente torrent μTorrent - está desenvolvendo uma variedade de aplicativos que utilizam o protocolo BitTorrent para coisas novas através do projeto BitTorrent Labs . Os experimentos de laboratório incluem um aplicativo de sincronização que sincroniza arquivos de forma segura entre vários computadores, transferindo os arquivos diretamente via BitTorrent e um experimento BitTorrent Live que usa o protocolo BitTorrent para transmitir transmissão ao vivo, transmissão de vídeo, aproveitando o poder do BitTorrent para transmitir vídeos ao vivo para grandes Números de pessoas sem os requisitos atuais de largura de banda.

![Alt text](https://preview.ibb.co/iGuuYQ/6starcraft_2_peer_to_peer_download_feature.jpg)
 
O BitTorrent pode ser usado principalmente para pirataria no momento, pois sua natureza descentralizada e ponto a ponto é uma resposta direta aos esforços para reprimir o Napster e outras redes peer-to-peer com pontos centrais de falha. No entanto, o BitTorrent é uma ferramenta com usos legítimos no presente - e muitos outros usos potenciais no futuro.
 
 
 
 
 
### Referências:
 

P2P Networking and Applications - Morgan Kaufmann, 2009 -Páginas 144-147

http://jonas.nitro.dk/bittorrent/bittorrent-rfc.html
 
http://www.bittorrent.org/beps/bep_0003.html
 
https://tools.ietf.org/html/rfc5694
 
https://www.howtogeek.com/141257/htg-explains-how-does-bittorrent-work/
 
http://computer.howstuffworks.com/bittorrent.htm

https://wiki.theory.org/index.php/BitTorrentSpecification
 
 
